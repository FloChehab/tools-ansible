SHELL = /bin/bash

.DEFAULT_GOAL := all

## help: Display list of commands (from gazr.io)
.PHONY: help
help: Makefile
	@sed -n 's|^##||p' $< | column -t -s ':' | sed -e 's|^| |'

## all: Run all targets
.PHONY: all
all: init format style test

## init: Bootstrap dev environment
.PHONY: init
init:
	pip install --user pre-commit
	pre-commit install
	cp play_dev.ex.yaml play_dev.yaml

## style: Check lint, code styling rules.
.PHONY: style
style: test

## format: Format code.
.PHONY: format
format:
	@printf "Not yet setup"

## test: Shortcut to launch all the test tasks
.PHONY: test
test:
	pre-commit run -a

## run_play_work: Run playbook to ignit current computer as my work computer
run_play_work:
	ansible-playbook play_work_computer.yaml --ask-become-pass --extra-vars "local_user=$(shell whoami)"

## run_play_home: Run playbook to ignit current computer as my home computer
run_play_home:
	ansible-playbook play_home_computer.yaml --ask-become-pass --extra-vars "local_user=$(shell whoami)"

## run_play_dev: Run dev playbook
run_play_dev:
	ansible-playbook play_dev.yaml --ask-become-pass --extra-vars "local_user=$(shell whoami)"
