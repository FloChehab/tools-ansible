# \[tools\] Ansible

## Setup

Install ansible: [https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Runinng

### Dev server

```bash
ansible-playbook play_devservers.yaml --extra-vars "target=all user_on_server=john" -i xx.xxx.xxx.xxx,
```

### Work computer

```bash
make run_play_work
```

### Home computer

```bash
make run_play_home
```

## Dev

### Setup

```bash
make init
```

### Running

To easily run some tasks edit [`play_dev.yaml`](./play_dev.yaml).

```bash
make run_play_dev
```
